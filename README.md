# 概述

基于 Spring Boot 2.x 的实例项目，用于演示如何使用 Spring Boot 2.x 构建一个 RESTful API 服务。

## 参考文档

* [Spring Boot 官方文档](https://docs.spring.io/spring-boot/docs/2.7.5/reference/htmlsingle/)
* [深入浅出 Spring Boot 2.x](https://item.jd.com/12403128.html)
