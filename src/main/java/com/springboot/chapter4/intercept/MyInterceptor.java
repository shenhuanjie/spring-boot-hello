package com.springboot.chapter4.intercept;


import com.springboot.chapter4.invoke.Invocation;

import java.lang.reflect.InvocationTargetException;

public class MyInterceptor implements Interceptor {


    /**
     * 事前方法
     */
    @Override
    public boolean before() {
        System.out.println("before ……");
        return true;
    }

    /**
     * 事后方法
     */
    @Override
    public void after() {
        System.out.println("after ……");
    }

    /**
     * 取代原有事件方法
     *
     * @param invocation ——回调参数，可以通过它的 proceed 方法，回调原有事件
     * @return {@link Object} 原有事件返回对象
     */
    @Override
    public Object around(Invocation invocation) throws InvocationTargetException, IllegalAccessException {
        System.out.println("around before ……");
        Object obj = invocation.proceed();
        System.out.println("around after ……");
        return obj;
    }

    /**
     * 是否返回方法。事件没有发生异常执行
     */
    @Override
    public void afterReturning() {
        System.out.println("afterReturning ……");
    }

    /**
     * 事后异常方法，当事件发生异常后执行
     */
    @Override
    public void afterThrowing() {
        System.out.println("afterThrowing ……");
    }

    /**
     * 是否使用 around 方法取代原有方法
     */
    @Override
    public boolean userAround() {
        return true;
    }
}
