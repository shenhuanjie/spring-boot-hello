package com.springboot.chapter4.service;

public interface HelloService {
    void sayHello(String name);
}
