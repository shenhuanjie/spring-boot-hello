package com.springboot.chapter4;

import com.springboot.chapter4.intercept.MyInterceptor;
import com.springboot.chapter4.service.HelloService;
import com.springboot.chapter4.service.impl.HelloServiceImpl;

public class Main {
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    private static void testProxy() {
        HelloService helloService = new HelloServiceImpl();
        // 按约定获取 proxy
        HelloService proxy = (HelloService) ProxyBean.getProxyBean(helloService, new MyInterceptor());
        // 调用代理的方法
        proxy.sayHello("world");
        System.out.println("\n########## name is null ##########\n");
        proxy.sayHello(null);

    }
}
