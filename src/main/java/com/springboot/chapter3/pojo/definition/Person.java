package com.springboot.chapter3.pojo.definition;

public interface Person {
    // 使用动物服务
    void service();

    // 设置动物
    void setAnimal(Animal animal);
}
