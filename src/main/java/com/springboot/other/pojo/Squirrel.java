package com.springboot.other.pojo;

import com.springboot.chapter3.pojo.definition.Animal;

public class Squirrel implements Animal {
    @Override
    public void use() {
        System.out.println("松鼠【" + Squirrel.class.getSimpleName() + "】是松树的天敌。");
    }
}
